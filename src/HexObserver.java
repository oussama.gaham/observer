public class HexObserver extends Observer {
	public HexObserver(Subject subject) {
		super(subject);
	}

	@Override
	public void update() {
		System.out.printf("Hex: %s\n", Integer.toHexString(subject.getState()));
	}
}
