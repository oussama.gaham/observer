public abstract class Observer {
	Subject subject;

	public Observer(Subject subject) {
		this.subject = subject;
	}

	public abstract void update();
}
