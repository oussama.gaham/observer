public class BinaryObserver extends Observer {
	public BinaryObserver(Subject subject) {
		super(subject);
	}

	@Override
	public void update() {
		System.out.printf("Binary: %s\n", Integer.toBinaryString(subject.getState()));
	}
}
