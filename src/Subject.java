import java.util.ArrayList;
import java.util.List;

public class Subject {
	List<Observer> observers = new ArrayList<>();
	int state;

	public void attach(Observer o) {
		this.observers.add(o);
	}

	public void notifyAllObservers(){
		for(Observer o : observers)
			o.update();
	}

	public void setState(int state) {
		System.out.printf("Decimal: %s\n", state);
		this.state = state;
		notifyAllObservers();
	}

	public int getState() {
		return state;
	}
}
