public class ObserverPatternDemo {
	public static void main(String[] args) {
		Subject subject = new Subject();

		subject.attach(new BinaryObserver(subject));
		subject.attach(new OctalObserver(subject));
		subject.attach(new HexObserver(subject));

		subject.setState(25);
	}
}
