public class OctalObserver extends Observer {
	public OctalObserver(Subject subject) {
		super(subject);
	}

	@Override
	public void update() {
		System.out.printf("Octal: %s\n", Integer.toOctalString(subject.getState()));
	}
}
